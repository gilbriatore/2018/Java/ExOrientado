package br.edu.up;


import br.edu.up.dominio.Carro;
import br.edu.up.dominio.Pessoa;

public class Programa 
{

	public static void main(String[] args)
	{
		Carro carro1 = new Carro("Chevrolet","Chevette","Hatch", 1978, 25, 10);
		Carro carro2 = new Carro("Chevrolet","Monza","Hatch", 1988, 33, 8);
		Carro carro3 = new Carro("Volkswagen","Fusca","Hatch", 1950, 10, 12);
		
		Pessoa pessoa1 = new Pessoa("Szostakio", "Vermelh�o", 53, "M");
		
		pessoa1.carros[0] = carro1;
		pessoa1.carros[1] = carro2;							
		pessoa1.carros[2] = carro3;
		
		System.out.println(pessoa1.nome);
		
		System.out.print("Autonomia carro 1: ");
		System.out.println(carro1.calcularAutonomia());
		
		System.out.print("Autonomia carro 2: ");
		System.out.println(carro2.calcularAutonomia());
				
		System.out.print("Autonomia carro 3: ");
		System.out.println(carro3.calcularAutonomia());
		
		//1. Fazer repeti��o e imprimir nome		
//		for (int i = 0; i < pessoas.length; i++) {
//			System.out.println(pessoas[i].nome);
//		}
		
		//2. Imprimir modelo carros > 1970		
//		for (int i = 0;i <pessoas.length; i++) {
//			if(pessoas[i].carro.ano > 1970) {
//				System.out.println(pessoas[i].carro.modelo);
//			}
//		}
		
		//3. Imprimir o nome do dono do carro < 1970
//		 for (int i= 0; i < pessoas.length; i++) {
//			 if(pessoas[i].carro.ano < 1970) {
//				 System.out.println(pessoas[i].nome);				
//			 }			 
//		 }		
	}		
}