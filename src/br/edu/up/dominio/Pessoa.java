package br.edu.up.dominio;

public class Pessoa 
{
	
	public String nome;
	public String cor;
	public String sexo;
	public int idade;
	public Carro[] carros = new Carro[3];
	
	public Pessoa(String nome, String cor, int idade, String sexo) 
	{
		this.nome  = nome;
		this.cor   = cor;
		this.idade = idade;
		this.sexo  = sexo;
	}
}